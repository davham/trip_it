class TripsController < ApplicationController

  helper_method :trips

  def index
    @trip = Trip.new
  end

  def create
    @trip = Trip.new trip_params
    return render 'index' if @trip.invalid?
    response = @trip.calculate
    unless response.nil?
      @trip.distance = response[:distance]
      @trip.duration = response[:duration]
    end
    
    if @trip.save
      redirect_to :trips
    else
      render 'index'
    end
  end

  def destroy
    @trip = Trip.find(params[:id])
    @trip.destroy

    redirect_to root_path, status: 303
  end

  private
  def trip_params
    params.require(:trip).permit(:start_point, :end_point)
  end

  def trips
    @trips ||= Trip.all.list
  end
end
