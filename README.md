# Trip It README

This is a sample rails application using Google Maps Directions API to calculate trip distance and duration for two locations entered by the user. The results will be saved to a Postgresql database and display all the previous trips entered.

* Ruby version `3.1.0`

* Rails version `7.0.1`

* System dependencies <br>
  Postgresql <br>
  `gem "bundler"`
  `gem "slim-rails"` <br>
  `gem "bootstrap", "~> 5.1.3"` <br>
  `gem "simple_form"` <br>
  `gem "dotenv-rails"` <br>

* Configuration <br>
  Set ruby version to `3.1.0`, I used rbenv. <br>
  rbenv: <br>
  &ensp;&ensp; `rbenv install 3.1.0` <br>
  &ensp;&ensp; `rbenv local 3.1.0` <br>
  RVM: <br> 
  &ensp;&ensp; `rvm install ruby-3.1.0` <br>
  &ensp;&ensp; `rvm use ruby-3.1.0` <br>
  Run `bundle install` to install dependencies. <br>
  Use of Google Maps Directions API requires an api key. <br>
  A `.env` file in the root of the project with the api key is required. <br>
    ```
      // file .env
      GMAPS_API_KEY=<api key here>`
    ```
  Create the database and migrate. <br>
  `rails db:create && rails db:migrate` <br>
  Start the server. <br>
  `rails s`
  
* How to run the test suite <br>
  Minitest with fixtures. <br>
  run `rake test` to run all tests <br>
