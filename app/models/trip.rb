require 'net/http'

class Trip < ApplicationRecord

  scope :list, -> { order(created_at: :desc) }

  validates :start_point, :end_point, presence: true

  def origin
    URI.encode_www_form_component start_point
  end
  
  def destination
    URI.encode_www_form_component end_point
  end

  def uri
    api_key = ENV['GMAPS_API_KEY']
    gmaps_url = "https://maps.googleapis.com/maps/api/directions/json?"
    params = "origin=#{origin}&destination=#{destination}&key=#{api_key}&mode=driving&avoid=tolls"
    URI(gmaps_url + params)
  end

  # Use google maps api to get duration and distance
  # t = Trip.new(start_point: "san francisco", end_point: "new york")
  # t.calculate
  def calculate
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    request = Net::HTTP.get_response(uri)
    response = JSON.parse(request.body)
    unless response['status'] == 'ZERO_RESULTS' || response['error_message']
      first_leg = response['routes'].first['legs'].first
      data = { 
        distance: first_leg['distance']['text'],
        duration: first_leg['duration']['text']
      }
    end
  end
end
