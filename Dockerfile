FROM ruby:3.1.0
RUN apt-get update -qq \
    && apt-get install -y nodejs postgresql-client libxml2-dev libxslt-dev
ADD . /app
WORKDIR /app
RUN bundle config build.nokogiri --use-system-libraries
RUN bundle install
EXPOSE 3000
CMD ["bash"]