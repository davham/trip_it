require "test_helper"

class TripsControllerTest < ActionDispatch::IntegrationTest
  setup do
  end
  
  test "should be success at index" do
    get trips_url
    assert_response :success
  end

  test "should create a trip" do
    assert_difference("Trip.count")do
      post "/trips", params: { 
        trip: { start_point: "Austin", end_point: "Dallas" }
      }
    end
    assert_redirected_to trips_path
  end

  test "should destroy a trip" do
    trip = trips(:long_distance)
    assert_difference "Trip.count", -1 do
      delete trip_path(trip)
    end
    assert_redirected_to root_path
  end
end
