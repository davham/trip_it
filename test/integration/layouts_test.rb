require "test_helper"

class LayoutsTest < ActionDispatch::IntegrationTest
  test "index should have a form submit button" do
    get root_path
    assert_select "input", value: "Create Trip"
  end

  test "index should have a table when trips exist" do
    trip = trips(:long_distance)
    get root_path
    assert_select "table.table"
  end

  test "index should have no table with no trips" do
    Trip.delete_all
    get root_path
    assert_select "table.table", false
  end
end
